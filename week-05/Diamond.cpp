#include<iostream>
using namespace std;
class  child { //class Person
public:
    void person()
    {
        cout << "Iam a person" << endl;
    }
};
 
class parent1 : public child { //class Father inherits Person
public:
    void father()
    {
        cout<<"father of person"<<endl;
    }
};
 
class parent2 : public child { //class Mother inherits Person
public:
    void mother()
    {
        cout<<"mother of person"<<endl;
    }
};
 
class Son : public parent1, public parent2  { //Child inherits Father and Mother
    public:
    void child()
    {
        cout<<"iam a child"<<endl;
    }
};
 
int main() {
    Son obj;
    obj.child();
}
