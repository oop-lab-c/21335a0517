 
#include<iostream>
using namespace std;
class base{
    private:
    int X=10;
    public:
    int Y=20;
    protected:
    int Z=30;
    int getX() {
        return X;
    }
    int getY() {
        return Y;
    }
    int getZ() {
        return Z;
    }
};
class publicclass: public base
{
    public:
    int getZ()
    {
        return Z;
    }
};
class protectedclass:protected base
{
    public:
    int getY()
    {
        return Y;
    }
    int getZ()
    {
        return Z;
    }
};
class privatedclass: private base
{
     public:
    int getY()
    {
        return Y;
    }
    int getZ()
    {
        return Z;
    }
};
int main()
{
    publicclass obj1;
    cout<<"....PUBLIC INHERITANCE...."<<endl;
    //cout << "Private = " << obj1.getX() << endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj1.getZ() << endl;
    cout << "Public = " << obj1.Y << endl;
    protectedclass obj2;
    cout<<"....PROTECTED INHERITANCE...."<<endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj2.getZ() << endl;
    cout << "Public = " << obj2.getY() << endl;
    privatedclass obj3;
    cout<<"....PRIVATE INHERITANCE...."<<endl;
    cout<<"cannot be accesed"<<endl;
    cout << "Protected = " << obj3.getZ() << endl;
    cout << "Public = " << obj3.getY() << endl;
    return 0;
}
