#include<iostream>
using namespace std;
class parent1
{
    public:
    parent1()
    {
        cout<<"hii"<<endl;
    }
};
class parent2
{
    public:
    parent2()
    {
        cout<<"hello "<< endl;
    }
};
class son1:public parent1 //simple inheritance
{
    public:
    son1()
    {
        cout<<"i am the younger one of the family"<<endl;
    }
};
class son2:public parent1,public parent2 //multiple inheritance
{
    public:
};
class son3:public son1//multilevel inheritance
{
    public:
};
class son4:public parent1 //hiracheal inheritance
{
    public:
};
class son5:public parent1 //hiracheal inheritance
{
    public:
};
int main()
{
    son1 obj1;
    son2 obj2;
    son3 obj3;
    son4 obj4;
    son5 obj5;
    return 0;
}
