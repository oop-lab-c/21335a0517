#include<iostream>
using namespace std;
class person
{
    public:
    person()
    {
        cout<<"call to mom"<<endl;
    }
};
class parent1:virtual public person
{
    public:
    parent1()
    {
        cout<<"call to dad"<<endl;
    }
};
class parent2: virtual public person
{
    public:
    parent2()
    {
        cout<<"call to bro"<<endl;
    }
};
class child:public parent1,public parent2
{
    public:
    child()
    {
        cout<<"call sucessful"<<endl;
    }
};
int main()
{
    child obj;
    return 0;
}
