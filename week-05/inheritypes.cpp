#include<iostream>
using namespace std;
class base1
{
    public:
    void parent1()
    {
        cout<<"Hello Parent1"<<endl;
    }
};
class base2
{
    public:
    void parent2()
    {
        cout<<"Hello Parent1"<<endl;
    }
};
//SIMPLE INHERITANCE
class son1: public base1
{
    public:
    void children1()
    {
        cout<<"Hi this is child1 ,Iam inherited from base1"<<endl;
    }
};
//MULTIPLE INHERITANCE
class son2: public base1,public base2
{
    public:
    void children2()
    {
        cout<<"Hi this child2,Iam inherited from base1 and 2"<<endl;
    }
};
//MULTI LEVEL INHERITANCE
class son3: public base1
{
    public:
    void children3()
    {
        cout<<"Hi this child3,Iam inherited from base1"<<endl;
    }
};
class son4: public son3
{
    public:
    void children4()
    {
        cout<<"Hi this child4,Iam inherited from child3 and child3 is inherited from base1"<<endl;
    }
};
//Hierachical INHERITANCE
class son5: public base2
{
    public:
    void children5()
    {
        cout<<"Hi this child5,Iam inherited from base1"<<endl;
    }
};
class son6: public base2
{
    public:
    void children6()
    {
        cout<<"Hi this child6,Iam inherited from base1"<<endl;
    }
};
int main()
{
    cout<<"..Simple Inheritance.."<<endl;
    son1 obj1;
    obj1.children1();
    obj1.parent1();
    cout<<"..Multiple Inheritance.."<<endl;
    son2 obj2;
    obj2.children2();
    obj2.parent1();
    obj2.parent2();
    cout<<"..Multi Level inheritance.."<<endl;
    son4 obj4;
    obj4.children4();
    obj4.children4();
    obj4.parent1();
    cout<<"..Hierachical Inhertance.."<<endl;
    son5 obj5;
    son5 obj6;
    obj5.children5();
    obj5.parent2();
    return 0;
}
