import java.util.*;
interface example
{
    void show();

}
abstract class base
{
    base()
    {
        System.out.println("this is a abstract class constructor:\n");
    }
    abstract void print();
    void wish()
    {
        System.out.println("hello everyone\n");
    }
}
class hi implements example
{
    public void show()
    {
        System.out.println("welcome to java");
    }
}
class abs extends base
{
    void print()
    {
        System.out.println("java is subject of cse\n");
    }
    public static void main(String[] args)
    {
        hi h=new hi();
        abs i=new abs();
        i.wish();
        h.show();
        i.print();
    }

}
