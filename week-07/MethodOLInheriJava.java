import java.util.*;
class parent {
    public void add(int x, int y) {
        System.out.println("sum of two numbers = "+(x+y));
    }
}

class child extends parent {
    public void add(int x, int y,int z) {
        System.out.println("sum of three numbers = "+(x+y+z));
    }
    
    public static void main(String args[]) {
        child obj = new child();
        obj.add(20,30);
        obj.add(42,30,16);
    
    }
}


